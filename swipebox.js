/**
 * @file
 * Integration file for swipebox module.
 */
(function($) {
  Drupal.behaviors.swipebox = {
    attach: function (context, settings) {
      $('.swipebox').swipebox({
        useCSS : true, // false will force the use of jQuery for animations.
        useSVG : true, // false to force the use of png for buttons.
        hideBarsOnMobile : false, // false will show the caption and navbar on mobile devices.
        hideBarsDelay : 3000, // delay before hiding bars.
        videoMaxWidth : 1140, // videos max width.
        beforeOpen: function() {}, // called before opening.
        afterClose: function() {} // called after closing.
      });
    }
  };
})(jQuery);
